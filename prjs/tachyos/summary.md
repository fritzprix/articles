## TachyOS

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/bc5b451d38624e44a58c8fd4913bc4a5)](https://www.codacy.com/app/innocentevil0914/TachyOS?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=fritzprix/TachyOS&amp;utm_campaign=Badge_Grade) [![Build Status](https://travis-ci.org/fritzprix/TachyOS.svg?branch=master)](https://travis-ci.org/fritzprix/TachyOS)

> TachyOS is the RTOS based on microkernel architecture which includes only minimal components like thread / synchronization, memory management, inter-thread communication while supporting execution context / address space isolation(protection) and extensible modular interface. Name of this project comes from the hypothetical particle called 'tachyon' [tækiɒn], whose speed increases as its energy decreases theoretically, is thought to be the ideal of the real time application which runs in very constrained environment.
